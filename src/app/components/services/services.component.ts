import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ModalServicesComponent } from '@modals/modal-services/modal-services.component';

@Component( {
    selector: 'app-services',
    templateUrl: './services.component.html',
    styleUrls: ['./services.component.scss']
} )
export class ServicesComponent implements OnInit {

    constructor( private modalService: BsModalService ) { }

    ngOnInit() {
    }

    showImages( num: number ) {
        let initialState = {};
        switch ( num ) {
            case 0:
                initialState = {
                    links: [
                        './assets/images/formalizacion/formalizacion-01.png',
                        './assets/images/formalizacion/formalizacion-02.png',
                        './assets/images/formalizacion/formalizacion-03.png',
                        './assets/images/formalizacion/formalizacion-04.png',
                        './assets/images/formalizacion/formalizacion-05.png',
                        './assets/images/formalizacion/formalizacion-06.png'
                    ]
                };
                break;
            case 1:
                initialState = {
                    links: [
                        './assets/images/digitalizacion/digitalizacion-01.png',
                        './assets/images/digitalizacion/digitalizacion-02.png',
                        './assets/images/digitalizacion/digitalizacion-03.png',
                        './assets/images/digitalizacion/digitalizacion-04.png',
                        './assets/images/digitalizacion/digitalizacion-05.png'
                    ]
                };
                break;
            case 2:
                initialState = {
                    links: [
                        './assets/images/desarrollo/productivo-01.png',
                        './assets/images/desarrollo/productivo-02.png',
                        './assets/images/desarrollo/productivo-03.png',
                        './assets/images/desarrollo/productivo-04.png',
                        './assets/images/desarrollo/productivo-05.png',
                        './assets/images/desarrollo/productivo-06.png'
                    ]
                };
                break;
            case 3:
                initialState = {
                    links: [
                        './assets/images/asesoria/asesoria-01.png',
                        './assets/images/asesoria/asesoria-02.png',
                        './assets/images/asesoria/asesoria-03.png',
                        './assets/images/asesoria/asesoria-04.png',
                        './assets/images/asesoria/asesoria-05.png'
                    ]
                };
                break;
            case 4:
                initialState = {
                    links: [
                        './assets/images/gestion/gestion-01.png',
                        './assets/images/gestion/gestion-02.png',
                        './assets/images/gestion/gestion-03.png',
                        './assets/images/gestion/gestion-04.png',
                        './assets/images/gestion/gestion-05.png'
                    ]
                };
                break;

            default:
                break;
        }

        const modal = this.modalService.show( ModalServicesComponent, {
            initialState,
            class: 'ModalService modal-dialog-centered',
        } );
    }

}
