import { Component, OnInit } from '@angular/core';
import { ScrollService } from '../../../services/scroll.service';

declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private scrollService: ScrollService) { }

  ngOnInit() {
  }

  close() {
    const hamburger = $('.hamburger');
    hamburger.click();
    hamburger.toggleClass('focus');
  }

  moveTo(n: number) {
    this.scrollService.position = n;
  }

   openNav() {
    document.getElementById("mySidenav").style.width = "250px";
  }
  
   closeNav() {
    document.getElementById("mySidenav").style.width = "0";
  }

}
