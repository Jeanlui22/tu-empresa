import { AfterViewInit, Component, OnInit } from '@angular/core';
import * as am4core from '@amcharts/amcharts4/core';
import * as am4maps from '@amcharts/amcharts4/maps';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import { element, ElementFinder } from 'protractor';

am4core.useTheme(am4themes_animated);
declare var am4geodata_peruLow: any;

interface City {
  id: string;
  name?: string;
  customName?: string;
  list?: boolean;
  direccion?: string[];
  togglable?: boolean;
  hoverable?: boolean;
  clickable?: boolean;
  fill?: any;
}


@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.scss']
})
export class SitesComponent implements OnInit, AfterViewInit {

  chart: am4maps.MapChart;
  polygonSeries = new am4maps.MapPolygonSeries();
  selected: string;
  cities: City[] = [];
  availableCities: City[] = [];
  lastSelected: any;
  selectedCity: City;
  placeholder = 'Buscar';

  constructor() { }

  ngOnInit() {
    this.getCities();
    this.loadMap();
  
  }

  ngAfterViewInit(): void {
   
  }

  downloadSedes() {
    window.open('http://www.orimi.com/pdf-test.pdf', '_blank');
  }

  loadMap() {
    const chart = am4core.create( 'chartdiv', am4maps.MapChart );

    chart.geodata = am4geodata_peruLow;

    chart.chartContainer.wheelable = false;
    chart.seriesContainer.draggable = false;
    chart.seriesContainer.events.disableType( 'doublehit' );
    chart.chartContainer.background.events.disableType( 'doublehit' );

    chart.projection = new am4maps.projections.Miller();
    this.polygonSeries = chart.series.push( new am4maps.MapPolygonSeries() );
    this.polygonSeries.useGeodata = true;

    this.polygonSeries.data = this.cities;

    const polygonTemplate = this.polygonSeries.mapPolygons.template;

    polygonTemplate.togglable = true;
    polygonTemplate.tooltipText = '{name}';

    polygonTemplate.propertyFields.fill = 'fill';
    polygonTemplate.propertyFields.togglable = 'togglable';
    polygonTemplate.propertyFields.hoverable = 'hoverable';
    polygonTemplate.propertyFields.clickable = 'clickable';

    let lastSelected: any;
    polygonTemplate.events.on( 'hit', ( ev: any ) => {
        try {
          
          this.select(ev.target.dataItem.dataContext.id);
      

            if ( this.lastSelected && this.lastSelected.click ) {
                this.cities.forEach( d => {
                    if ( d.fill ) {
                        console.log( 'fill' );
                        d.fill = am4core.color( '#00A6E0' );
                    }
                } );
                this.polygonSeries.data = this.cities;
                this.lastSelected.click = false;
                
            }

            if ( lastSelected ) {
                lastSelected.isActive = false;
            }

            if ( lastSelected !== ev.target ) {
                lastSelected = ev.target;
               
            }

            this.selected = ev.target.dataItem.dataContext.id;
            if ( this.selected === 'PE-LMA' ) {
                this.selected = 'PE-LIM';
               
            }
        } catch ( e ) {
            this.select( ev.target.dataItem.dataContext.id );
        }

    } );

    /* Create selected and hover states and set alternative fill color */
    const ss = polygonTemplate.states.create( 'active' );
    ss.properties.fill = am4core.color( '#E1251B' );

    const hs = polygonTemplate.states.create( 'hover' );
    hs.properties.fill = am4core.color( '#E1251B' );


    this.chart = chart;
}

  getCities() {
    this.cities = [
        {
            id: 'PE-PIU',
            name: 'Piura',
            direccion: ['Av. Sánchez Cerro N° 234 Mz 239 Lt A Lc 145 A TI 01 CC Real Plaza - Zona industrial antigua Centro Comercial MAC - Piura Módulo 37'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-CAJ',
            name: 'Cajamarca',
            direccion: ['Ca. Inca Garcilazo de la Vega S/N, cuadra 9. (Ref. Municipalidad Provincial de Jaén, altura del Estadio Municipal de Jaén)','Jr.Atahualpa S/N, cuadra 4 (Próximo a cambiarse de dirección)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-AMA',
            name: 'Amazonas',
            direccion: ['Jr. Leoncio Prado N° 429'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-LAL',
            name: 'La Libertad',
            direccion: ['Jirón Diego de Almagro N° 525','Av. Víctor Larco 1212 - Trujillo','Av. Víctor Larco 1212 - Trujillo'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-SAM',
            name: 'San Martín',
            direccion: ['Jr. Patrón Santiago N° 119'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-ANC',
            name: 'Ancash',
            direccion:['Av. Pacífico S/N (Ref. Instituto de Educación Superior Tecnológico Público Carlos S. Romero)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-HUC',
            name: 'Huánuco',
            direccion: ['Av. Alameda Perú N° 550, Tingo María (Ref. Dirección Regional Agraria del Gobierno Regional de Huánuco)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-ARE',
            name: 'Arequipa',
            direccion: ['Av. Independencia S/N (Ref. Universidad Nacional San Agustin, 1er piso Pabellon Nicholson)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-JUN',
            name: 'Junín',
            direccion: ['Av. Mariscal Castilla N° 3909 (Edificio Inteligente 1er piso de la Universidad Nacional del Centro del Perú)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-UCA',
            name: 'Ucayali',
            direccion: ['Carretera Federico Basadre km. 5.8, Pucallpa (Ref. Instituto de Educación Superior Tecnológico Público Suiza)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-AYA',
            name: 'Ayacucho',
            direccion: ['Jr. Cuzco S/N cuadra. 1 (Ref. Municipalidad Huamanga)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-CUS',
            name: 'Cusco',
            direccion: ['Av. César Vallejo S/N, Plaza principal Pichari - La Convención - Zona Vraem (Ref. Municipalidad Distrital de Pichari)','Ca. Túpac Amaru S/N (Ref. Esq. con Av. Micaela Bastidas, costado de local de la Municipalidad Wanchaq)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-PUN',
            name: 'Puno',
            direccion: ['Jr. Andahuaylas N° 239, Barrio Bellavista'],
            list: true,
            fill: am4core.color( '#00A6E0' )
        },
        {
            id: 'PE-MDD',
            name: 'Madre de Dios',
            direccion: ['pr. Calle Ancash S/N (Ref. Universidad Nacional de Moquegua)'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-MOQ',
            name: 'Moquegua',
            direccion: ['Pr. Calle Ancash S/N (Ref. Universidad Nacional de Moquegua)','Jr. Mirave N° 212 interior puesto 2 - Mercado Pacocha'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-TAC',
            name: 'Tacna',
            direccion: ['Av. Prolongación Pinto N° 1337'],
            list: true,
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-LIM',
            name: 'Lima Region',
            direccion: ['Av. Guardia Civil N° 834','Jiron Huánuco N° 1701 Piso 5'],
            customName: 'Lima',
            list: true,
            fill: am4core.color( '#00A6E0' ),

        },
        {
            id: 'PE-LMA',
            name: 'Lima Provincia',
            direccion: ['Av. Guardia Civil N° 834','Jiron Huánuco N° 1701 Piso 5'],
            fill: am4core.color( '#00A6E0' ),
        },
        {
            id: 'PE-TUM',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-LOR',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-LAM',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-PAS',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-HUV',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-CAL',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-ICA',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-APU',
            togglable: false,
            hoverable: false,
            clickable: false
        },
        {
            id: 'PE-LKT',
            togglable: false,
            hoverable: false,
            clickable: false
        },
    ];

    this.availableCities = this.cities.filter( ( c ) => c.list );
}

  select( id: string, click = true ) {
    this.cities.forEach( d => {
        if ( d.id === id || ( id === 'PE-LIM' && d.id === 'PE-LMA' ) ) {
            this.selectedCity = d;
            this.placeholder = this.selectedCity.name;
            d.fill = am4core.color( '#E1251B' );
        } else if ( d.fill ) {
            d.fill = am4core.color( '#00A6E0' );
        }
    } );

    this.polygonSeries.data = this.cities;
    this.selected = id;

    this.lastSelected = {
        click
    };

}

}
