import { Component, HostListener, OnInit, AfterViewInit } from '@angular/core';
import { ScrollService } from '../../services/scroll.service';

declare var $: any

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

  sections = ['#header','#conocenos','#servicios','#sites','#footer'];
  scrolling = false;
  constructor( private scrollService: ScrollService) { }

  ngOnInit() {
    this.goToSection( this.sections[ this.scrollService.position ] );
  }

  ngAfterViewInit(){
    this.goToSection( this.sections[ this.scrollService.position ] );
  }

  @HostListener( 'window:mousewheel', ['$event'] )
  onWindowScroll( e ) {
      if ( !this.scrolling ) {
          if ( e.wheelDelta >= 0 ) {
              if ( this.scrollService.position > 0 ) {
                  this.scrollService.position--;
              }
          } else {
              if ( this.scrollService.position < this.sections.length - 1 ) {
                  this.scrollService.position++;
              }
          }
          this.goToSection( this.sections[ this.scrollService.position ] );
      }
  }

  @HostListener( 'window:keyup', ['$event'] )
  onWindowKeyUp( e ) {
      if ( !this.scrolling && ( e.code === 'ArrowDown' || e.code === 'ArrowUp' ) ) {
          if ( e.code === 'ArrowUp' ) {
              if ( this.scrollService.position > 0 ) {
                  this.scrollService.position--;
              }
          } else if ( e.code === 'ArrowDown' ) {
              if ( this.scrollService.position < this.sections.length - 1 ) {
                  this.scrollService.position++;
              }
          }
          this.goToSection( this.sections[ this.scrollService.position ] );
      }
  }

  goToSection( section: string ) {
      this.scrolling = true;
      $( 'html, section' ).stop().animate( {
          scrollTop: $( section ).offset().top
      }, 100, () => {
          this.scrolling = false;
      } );

  }

}
