import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-top',
  templateUrl: './modal-top.component.html',
  styleUrls: ['./modal-top.component.scss']
})
export class ModalTopComponent implements OnInit {

  titulo: string;
  constructor( public bsModalRef: BsModalRef ) {}
 
  ngOnInit() {

  }
}