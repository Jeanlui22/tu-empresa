import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalTopComponent } from './modal-top.component';

describe('ModalTopComponent', () => {
  let component: ModalTopComponent;
  let fixture: ComponentFixture<ModalTopComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalTopComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalTopComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
