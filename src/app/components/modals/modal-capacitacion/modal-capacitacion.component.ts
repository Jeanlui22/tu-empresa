import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-capacitacion',
  templateUrl: './modal-capacitacion.component.html',
  styleUrls: ['./modal-capacitacion.component.scss']
})
export class ModalCapacitacionComponent implements OnInit {

  titulo: string;
  constructor( public bsModalRef: BsModalRef ) { }

  ngOnInit() {
  }

}
