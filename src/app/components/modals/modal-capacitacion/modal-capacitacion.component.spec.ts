import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalCapacitacionComponent } from './modal-capacitacion.component';

describe('ModalCapacitacionComponent', () => {
  let component: ModalCapacitacionComponent;
  let fixture: ComponentFixture<ModalCapacitacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalCapacitacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalCapacitacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
