import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalFeriasComponent } from './modal-ferias.component';

describe('ModalFeriasComponent', () => {
  let component: ModalFeriasComponent;
  let fixture: ComponentFixture<ModalFeriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalFeriasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalFeriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
