import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-ferias',
  templateUrl: './modal-ferias.component.html',
  styleUrls: ['./modal-ferias.component.scss']
})
export class ModalFeriasComponent implements OnInit {

  titulo: string;
  constructor( public bsModalRef: BsModalRef ) { }

  ngOnInit() {
  }

}
