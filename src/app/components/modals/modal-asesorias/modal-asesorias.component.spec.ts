import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalAsesoriasComponent } from './modal-asesorias.component';

describe('ModalAsesoriasComponent', () => {
  let component: ModalAsesoriasComponent;
  let fixture: ComponentFixture<ModalAsesoriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ModalAsesoriasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalAsesoriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
