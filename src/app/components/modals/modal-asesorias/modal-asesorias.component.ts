import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-asesorias',
  templateUrl: './modal-asesorias.component.html',
  styleUrls: ['./modal-asesorias.component.scss']
})
export class ModalAsesoriasComponent implements OnInit {

  titulo: string;
  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

  cerrar(){
    this.bsModalRef.hide()
  }
}
