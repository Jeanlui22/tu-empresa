import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap';

@Component({
  selector: 'app-modal-services',
  templateUrl: './modal-services.component.html',
  styleUrls: ['./modal-services.component.scss']
})
export class ModalServicesComponent implements OnInit {

  links: string[] = [];

  constructor(public bsModalRef: BsModalRef) { }

  ngOnInit() {
  }

}
