import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ModalModule } from 'ngx-bootstrap';
import { RouterModule } from '@angular/router';
import { ModalTopComponent } from './modal-top/modal-top.component';
import { ModalServicesComponent } from './modal-services/modal-services.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';
import { ModalCapacitacionComponent } from './modal-capacitacion/modal-capacitacion.component';
import { ModalAsesoriasComponent } from './modal-asesorias/modal-asesorias.component';
import { ModalFeriasComponent } from './modal-ferias/modal-ferias.component';



@NgModule({
 
  imports: [
    CommonModule,
    ModalModule.forRoot(),
    CarouselModule.forRoot(),
    RouterModule
  ],
  declarations: [
    ModalTopComponent,
    ModalServicesComponent,
    ModalCapacitacionComponent,
    ModalAsesoriasComponent,
    ModalFeriasComponent
  ],
  entryComponents:[
    ModalTopComponent,
    ModalServicesComponent,
    ModalCapacitacionComponent,
    ModalAsesoriasComponent,
    ModalFeriasComponent
  ]
  
})
export class ModalsModule { }
