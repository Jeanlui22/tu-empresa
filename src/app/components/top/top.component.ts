import { Component, OnInit } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { ModalAsesoriasComponent } from '@modals/modal-asesorias/modal-asesorias.component';
import { ModalCapacitacionComponent } from '@modals/modal-capacitacion/modal-capacitacion.component';
import { ModalFeriasComponent } from '@modals/modal-ferias/modal-ferias.component';

@Component({
  selector: 'app-top',
  templateUrl: './top.component.html',
  styleUrls: ['./top.component.scss']
})
export class TopComponent implements OnInit {
  bsModalRef: BsModalRef;

  constructor(  private modalService: BsModalService) { }

  ngOnInit() {
  }
  showModal(num:number) {
    switch (num) {
      case 1:
        const modal1 = this.modalService.show( ModalCapacitacionComponent, {
          class: 'ModalTop modal-dialog-centered '
        } );
        break;

      case 2:
        const modal2 = this.modalService.show( ModalAsesoriasComponent, { 
          class: 'ModalTop modal-dialog-centered '
        } );

        break;
    
      case 3:
        const modal3 = this.modalService.show( ModalFeriasComponent, {
          class: 'ModalTop modal-dialog-centered ',
        } );
        break;

      default:
        break;
    }


}

}



