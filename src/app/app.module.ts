import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';





import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { MeetUsComponent } from './components/meet-us/meet-us.component';
import { ServicesComponent } from './components/services/services.component';
import { SitesComponent } from './components/sites/sites.component';
import { TopComponent } from './components/top/top.component';
import { HeaderComponent } from './components/common/header/header.component';
import { FooterComponent } from './components/common/footer/footer.component';
import { CommonComponentsModule } from './components/common/common-components.module';
import { ModalsModule } from '@modals/modals.module';


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    MeetUsComponent,
    SitesComponent,
    ServicesComponent,
    TopComponent,
    HeaderComponent,
    FooterComponent
    
  ],
  imports: [
    
    BrowserModule,
    AppRoutingModule,
    CommonComponentsModule,
    ModalsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
